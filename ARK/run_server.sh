#!/bin/sh
cd /root/mounted
# Fixes [S_API FAIL] SteamAPI_Init() failed; unable to locate a running instance of Steam, or a local steamclient.dll.
export HOME="/root"
file="/root/.steam/sdk32/steamclient.so"
if ! [ -f "$file" ]
then
	mkdir -p ~/.steam/sdk32/
	ln -s /etc/service/steamcmd/linux32/steamclient.so "$file"
fi

# Start ARK server
exec /root/mounted/ShooterGame/Binaries/Linux/ShooterGameServer TheIsland?listen?SessionName=Bad_Dragon_Ark?ServerPassword=shiggy -server -log
