#!/bin/sh
cd /root/mounted
# Fixes [S_API FAIL] SteamAPI_Init() failed; unable to locate a running instance of Steam, or a local steamclient.dll.
export HOME="/root"
file="/root/.steam/sdk32/steamclient.so"
if ! [ -f "$file" ]
then
	mkdir -p ~/.steam/sdk32/
	ln -s /etc/service/steamcmd/linux32/steamclient.so "$file"
fi

# Start insurgency
exec ./srcds_run +maxplayers 49 +map prophet_coop checkpoint +mapcyclefile mapcycle_custom.txt -workshop -port 27016
